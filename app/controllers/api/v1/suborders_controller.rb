class Api::V1::SubordersController < Api::V1::BaseController
  respond_to :json

  # GET /suborders
  # GET /suborders.json
  def index
    respond_with Suborder.all
  end

  # GET /suborders/1
  # GET /suborders/1.json
  def show
    respond_with Suborder.find(params[:id])
  end

  # GET /suborders/new
  def create
    @suborders = Suborder.new(suborder_params)

    if @suborders.save
      render json: @suborders, status: 201
    else
      render json: { errors: @suborders.errors}, status: 422
    end
  end

  # GET /suborders/1/edit

  # PATCH/PUT /suborders/1
  # PATCH/PUT /suborders/1.json

  private
    # Use callbacks to share common setup or constraints between actions.

    # Never trust parameters from the scary internet, only allow the white list through.
    def suborder_params
      params.require(:suborder).permit(:start_date, :end_date, :total, :day, :company,:product_name, :email,:center)
    end
end
