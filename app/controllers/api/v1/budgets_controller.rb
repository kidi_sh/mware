class Api::V1::BudgetsController < Api::V1::BaseController
  respond_to :json
#get
  def index
    respond_with Budget.all
  end
#get
  def show
    respond_with Budget.find(params[:id])
  end
#post
  def create
    @budgets = Budget.new(budget_params)

    if @budgets.save
      render json: @budgets, status: 201
    else
      render json: { errors: @budgets.errors}, status: 422
    end
  end
#patch
  def update
    @budgets = Budget.find(params[:id])

    if @budgets.update(budget_params)
      render json: @budgets, status: 200
    else
      render json: { errors: @budgets.errors}, status: 422
    end
  end

  private

  def budget_params
    params.require(:budget).permit(:date, :id_request, :user_id, :department_role_id, :details, :quantity, :vendor, :amount, :cash_in, :cash_out, :balance,:action)
  end

end
