class OvertimesController < ApplicationController
  before_filter :authenticate_user!
  before_action :set_overtime, only: [:show, :edit, :update, :destroy]

  # GET /overtimes
  # GET /overtimes.json
  def index
    if current_user.department_id == 1 || current_user.department_id == 5 
      @overtimes = Overtime.all
    else
      @overtimes = Overtime.where('user_id = ?', current_user)
    end
  end

  # GET /overtimes/1
  # GET /overtimes/1.json
  def show
  end

  # GET /overtimes/new
  def new
    @overtime = Overtime.new
  end

  # GET /overtimes/1/edit
  def edit
  end

  # POST /overtimes
  # POST /overtimes.json
  def create
    @overtime = Overtime.new(overtime_params)
    @overtime.user_id = current_user.id
    @overtime.center_id = current_user.center_id
    @overtime.status = "1"
    @overtime.amount = ((Time.parse(params[:overtime][:end]) - Time.parse(params[:overtime][:date]))/3600 )* current_user.ot_fee
    respond_to do |format|
      if @overtime.save
        BudgetMailer.new_ot(@overtime).deliver_now
        # @overtime.amount = @overtime.end - @overtime.date
        format.html { redirect_to @overtime, notice: 'Overtime was successfully created.' }
        format.json { render :show, status: :created, location: @overtime }
      else
        format.html { render :new }
        format.json { render json: @overtime.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /overtimes/1
  # PATCH/PUT /overtimes/1.json
  def update
    respond_to do |format|
      if @overtime.update(overtime_params)
        if @overtime.status == "2"
          BudgetMailer.ot_approve(@overtime).deliver_now
        elsif @overtime.status == "3"
          BudgetMailer.rt_approve(@overtime).deliver_now
        end
        format.html { redirect_to @overtime, notice: 'Overtime was successfully updated.' }
        format.json { render :show, status: :ok, location: @overtime }
      else
        format.html { render :edit }
        format.json { render json: @overtime.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /overtimes/1
  # DELETE /overtimes/1.json
  def destroy
    @overtime.destroy
    respond_to do |format|
      format.html { redirect_to overtimes_url, notice: 'Overtime was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_overtime
      @overtime = Overtime.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def overtime_params
      params.require(:overtime).permit(:id, :date, :user_id, :note, :status, :center_id, :end)
    end
end
