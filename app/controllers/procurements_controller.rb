class ProcurementsController < ApplicationController
  before_action :set_procurement, only: [:show, :edit, :update, :destroy]

  # GET /procurements
  # GET /procurements.json
  def index
    if current_user.head == 1
      @request = Procurement.joins(:user).where('users.department_id = ? and status = ?', current_user.department_id, 1)
      @head = Procurement.joins(:user).where('users.department_id = ? and status = ?', current_user.department_id, 2)
      @procurement = Procurement.joins(:user).where('users.department_id = ? and status = ?', current_user.department_id, 3)
      @finance = Procurement.joins(:user).where('users.department_id = ? and status = ?', current_user.department_id, 4)
      @reject = Procurement.joins(:user).where('users.department_id = ? and status = ?', current_user.department_id, 5)
    else
      @all = Procurement.where('user_id = ?', current_user.id)
      @request = Procurement.where('user_id = ? and status = ?', current_user.id, 1)
      @head = Procurement.where('user_id = ? and status = ?', current_user.id, 2)
      @procurement = Procurement.where('user_id = ? and status = ?', current_user.id, 3)
      @finance = Procurement.where('user_id = ? and status = ?', current_user.id, 4)
      @reject = Procurement.where('user_id = ? and status = ?', current_user, 5)
    end

  end

  # GET /procurements/1
  # GET /procurements/1.json
  def show
  end

  # GET /procurements/new
  def new
    @procurement = Procurement.new
    @procurement.product_lists.build
    @procurement.status = 1
  end

  # GET /procurements/1/edit
  def edit
  end

  # POST /procurements
  # POST /procurements.json
  def create
    @procurement = Procurement.new(procurement_params)

    respond_to do |format|
      if @procurement.save
        format.html { redirect_to @procurement, notice: 'Procurement was successfully created.' }
        format.json { render :show, status: :created, location: @procurement }
      else
        format.html { render :new }
        format.json { render json: @procurement.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /procurements/1
  # PATCH/PUT /procurements/1.json
  def update
    respond_to do |format|
      if @procurement.update(procurement_params)
        format.html { redirect_to @procurement, notice: 'Procurement was successfully updated.' }
        format.json { render :show, status: :ok, location: @procurement }
      else
        format.html { render :edit }
        format.json { render json: @procurement.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /procurements/1
  # DELETE /procurements/1.json
  def destroy
    @procurement.destroy
    respond_to do |format|
      format.html { redirect_to procurements_url, notice: 'Procurement was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_procurement
      @procurement = Procurement.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def procurement_params
      params.require(:procurement).permit(:user_id, :center_id, :purpose, :status, product_lists_attributes:[:id, :name, :price, :link, :approve, :_destroy])
    end
end
