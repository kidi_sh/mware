class Overtimes::ApproveController < OvertimesController
  def index
    if current_user.department_id == 5
      @overtimes = Overtime.where('status = ?', "2")
    else
      @overtimes = Overtime.where('status = ? and user_id = ?',"2", current_user)
    end
  end
end
