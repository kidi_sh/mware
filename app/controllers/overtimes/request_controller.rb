class Overtimes::RequestController < OvertimesController
  def index
    if current_user.department_id == 5
      @overtimes = Overtime.where('status = ?',"1")
    else
      @overtimes = Overtime.where('status = ? and user_id = ?',"1", current_user)
    end
  end
end
