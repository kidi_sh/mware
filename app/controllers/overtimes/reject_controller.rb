class Overtimes::RejectController < OvertimesController
  def index
    if current_user.department_id == 5
      @overtimes = Overtime.where('status = ?',"3")
    else
      @overtimes = Overtime.where('status = ? and user_id = ?',"3", current_user)
    end
  end
end
