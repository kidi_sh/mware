class BudgetsController < ApplicationController
  before_filter :authenticate_user!
  #layout 'dashboard'
  before_action :set_budget, only: [:show, :edit, :update, :destroy]
  # GET /budgets
  # GET /budgets.json
  def index
    if current_user.department_id == 1 and current_user.clvl == 1
      @budgets = Budget.all
    elsif current_user.department_id == 1 and current_user.head == 1
      @budgets = Budget.all
    elsif current_user.department_id != 1 and current_user.head == 1
      @budgets = Budget.joins(:user).where('users.department_id = ?', current_user.department_id)
    else
      @budgets = Budget.where('user_id = ?', current_user)
    end
  end

  # GET /budgets/1
  # GET /budgets/1.json
  def show
  end

  # GET /budgets/new
  def new
    @budget = Budget.new
    @budget.items.build
    @budget.proofs.build
  end

  # GET /budgets/1/edit
  def edit
  end

  # POST /budgets
  # POST /budgets.json
  def create
    @budget = Budget.new(budget_params)
    if current_user.head != 1
      @budget.status = 1
    end
    @budget.user_id = current_user.id
    respond_to do |format|
      if @budget.save
        BudgetMailer.notify_finance(@budget).deliver_now
        format.html { redirect_to @budget, notice: 'Budget was successfully created.' }
        format.json { render :show, status: :created, location: @budget }
      else
        format.html { render :new }
        format.json { render json: @budget.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /budgets/1
  # PATCH/PUT /budgets/1.json
  def update
    respond_to do |format|
      if @budget.update(budget_params)
        if current_user.department.name == "Finance"
          if @budget.status != 1
            BudgetMailer.notify_user(@budget).deliver_now
          end
        end
        BudgetMailer.notify_finance(@budget).deliver_now
        format.html { redirect_to @budget, notice: 'Budget was successfully updated.' }
        format.json { render :show, status: :ok, location: @budget }
      else
        format.html { render :edit }
        format.json { render json: @budget.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /budgets/1
  # DELETE /budgets/1.json
  def destroy
    @budget.destroy
    respond_to do |format|
      format.html { redirect_to budgets_url, notice: 'Budget was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def send_jurnal
    # Budget.where(:id => params[:budget_ids]).update_all(status: 2)
    # redirect_to budgets_path
    @budget = Budget.where(:id => params[:budget_ids])
    BudgetMailer.trf(@budget).deliver_now
    respond_to do |format|
      format.html { redirect_to budgets_path, notice: "success" }
    end
  end

  def submit_jurnal
    @budget = Budget.find(params[:id])
    #to map each item entry
    ejournal = @budget.items.where('status = 1').map{|f| {:account_name => f.type_product, :description => f.product, :debit => (f.quantity * f.price).to_f }}
    #to get total journal
    tjournal =  ejournal.map{|s| s[:debit]}.reduce(0,:+).to_i
    djournal =  ejournal.map{|s| s[:description]}.join(' + ')
    cjournal =  [{ :account_name => 'Bank BCA (1)',  :credit => tjournal,  :description => djournal }]

    item_attr = ejournal + cjournal

    if Rails.env.production?
      url = URI("https://api.jurnal.id/core/api/v1/journal_entries?apikey=906dbd5f8453c9a323b6f9b056690c96")
    else
      url = URI("https://sandbox-api.jurnal.id/core/api/v1/journal_entries?apikey=c51a5dfab205e5030e4d7707d12dc42e")
    end

    http = Net::HTTP.new(url.host, url.port)
    http.use_ssl = true
    http.verify_mode = OpenSSL::SSL::VERIFY_NONE

    @payload = {
                "journal_entry": {
                  "transaction_date": @budget.date,
                  "transaction_account_lines_attributes": item_attr
                  }
                }.to_json

  request = Net::HTTP::Post.new(url)
  request["content-type"] = 'application/json'
  request.body = @payload

  response = http.request(request)
  puts response.read_body
  respond_to do |format|
    format.html { redirect_to @budget, notice: response.message }
  end

  end

  def fund_transfer
    @budget = Budget.find(params[:id])
    BudgetMailer.trf(@budget).deliver_now
    respond_to do |format|
      format.html { redirect_to @budget, notice: "success" }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_budget
      @budget = Budget.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def budget_params
      params.require(:budget).permit(:date, :id_request, :user_id, :department_role_id, :details, :quantity, :vendor, :amount, :cash_in, :cash_out,:note, :balance,:action,:status,proofs_attributes:[:image], items_attributes: [:id, :type_product, :product ,:vendor, :quantity, :price, :status, :_destroy])
    end
end
