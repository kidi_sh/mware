class DepartmentRolesController < ApplicationController
  before_filter :authenticate_user!
  before_action :set_department_role, only: [:show, :edit, :update, :destroy]

  # GET /department_roles
  # GET /department_roles.json
  def index
    @department_roles = DepartmentRole.all
  end

  # GET /department_roles/1
  # GET /department_roles/1.json
  def show
  end

  # GET /department_roles/new
  def new
    @department_role = DepartmentRole.new
  end

  # GET /department_roles/1/edit
  def edit
  end

  # POST /department_roles
  # POST /department_roles.json
  def create
    @department_role = DepartmentRole.new(department_role_params)

    respond_to do |format|
      if @department_role.save
        format.html { redirect_to @department_role, notice: 'Department role was successfully created.' }
        format.json { render :show, status: :created, location: @department_role }
      else
        format.html { render :new }
        format.json { render json: @department_role.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /department_roles/1
  # PATCH/PUT /department_roles/1.json
  def update
    respond_to do |format|
      if @department_role.update(department_role_params)
        format.html { redirect_to @department_role, notice: 'Department role was successfully updated.' }
        format.json { render :show, status: :ok, location: @department_role }
      else
        format.html { render :edit }
        format.json { render json: @department_role.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /department_roles/1
  # DELETE /department_roles/1.json
  def destroy
    @department_role.destroy
    respond_to do |format|
      format.html { redirect_to department_roles_url, notice: 'Department role was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_department_role
      @department_role = DepartmentRole.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def department_role_params
      params.require(:department_role).permit(:name, :department_id)
    end
end
