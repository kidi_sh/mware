class SubordersController < ApplicationController
  before_action :set_suborder, only: [:show, :edit, :update, :destroy]

  # GET /suborders
  # GET /suborders.json
  def index
    @suborders = Suborder.all
  end

  # GET /suborders/1
  # GET /suborders/1.json
  def show
  end

  # GET /suborders/new
  def new
    @suborder = Suborder.new
  end

  # GET /suborders/1/edit
  def edit
  end

  # POST /suborders
  # POST /suborders.json
  def create
    if (params[:suborder][:day]).to_i < 100
      if (params[:suborder][:day]).to_i > 1
        for i in 1..(params[:suborder][:day]).to_i  do
          @suborder = Suborder.new(suborder_params)
          @suborder.end_date = @suborder.start_date
          @suborder.day = 1
          @suborder.save
        end
        redirect_to suborders_path
      else
        @suborder = Suborder.new(suborder_params)
        @suborder.end_date = @suborder.start_date
        @suborder.save
        redirect_to @suborder
      end
    else
      redirect_to suborders_path, notice: 'Quantity cannot exceed'
    end
  end

  # PATCH/PUT /suborders/1
  # PATCH/PUT /suborders/1.json
  def update
    respond_to do |format|
      if @suborder.update(suborder_params)
        format.html { redirect_to @suborder, notice: 'Suborder was successfully updated.' }
        format.json { render :show, status: :ok, location: @suborder }
      else
        format.html { render :edit }
        format.json { render json: @suborder.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /suborders/1
  # DELETE /suborders/1.json
  def destroy
    @suborder.destroy
    respond_to do |format|
      format.html { redirect_to suborders_url, notice: 'Suborder was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def create_juser
    require 'uri'
    require 'net/http'

    if Rails.env.production?
      url = URI("https://api.jurnal.id/core/api/v1/customers?apikey=906dbd5f8453c9a323b6f9b056690c96")
    else
      url = URI("https://sandbox-api.jurnal.id/core/api/v1/customers?apikey=c51a5dfab205e5030e4d7707d12dc42e")
    end

    http = Net::HTTP.new(url.host, url.port)
    if Rails.env.production?
    http.use_ssl = true
    else
    http.use_ssl = true
    end
    http.verify_mode = OpenSSL::SSL::VERIFY_NONE

    ap = "Accounts Payable"
    ar = Center.where('tag = ?', @suborder.center).pluck("receivable").first

    @payload = {
        "customer": {
        "display_name": @suborder.company,
        "email": @suborder.email,
        "default_ar_account_name": ar,
        "default_ap_account_name": ap
                          } }.to_json

    request = Net::HTTP::Post.new(url)
    request["content-type"] = 'application/json'
    request.body = @payload

    response = http.request(request)
    puts response.read_body
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_suborder
      @suborder = Suborder.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def suborder_params
      params.require(:suborder).permit(:start_date, :end_date, :total, :day, :company, :product_name, :status, :email,:center,:discount)
    end
end
