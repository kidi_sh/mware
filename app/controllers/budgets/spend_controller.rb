class Budgets::SpendController < BudgetsController
  def index
    if current_user.department_id == 1
      @budgets = Budget.where('status = 4')
    else
      @budgets = Budget.where('status = 4 and user_id = ?', current_user)
    end
  end
end
