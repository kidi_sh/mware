class Budgets::ApproveController < BudgetsController
  def index
    if current_user.clvl == 1 && current_user.department_id == 1
      @budgets = Budget.where('status = 7')
    elsif  current_user.head == 1 && current_user.department_id == 1
      @budgets = Budget.where('status = 3')
    elsif current_user.head == 1 && current_user.department_id != 1
      @budgets = Budget.where('status = 6')
    else
      @budgets = Budget.where('status = 6 and user_id = ?', current_user)
    end
  end
end
