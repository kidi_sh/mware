class Budgets::ReviseController < BudgetsController
  def index
    if current_user.department_id == 2
      @budgets = Budget.where('status = 2')
    else
      @budgets = Budget.where('status = 2 and user_id = ?', current_user)
    end
  end

  def edit
  end
  
end
