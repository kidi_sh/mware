class PaymentReceiptController < ApplicationController
  include SearchesHelper

  def show
    @search = Search.find(params[:id])
    trans_builder
    trans_detail

    respond_to do |format|
      format.html do
        render template: "payment_receipt/show.html.erb"
      end
       format.pdf do
         render pdf: "invoice",
                template: "payment_receipt/show.pdf.erb",
                locals: {:search => @search},
                encoding: 'UTF-8'
       end
    end
  end

  def send_payment
    @search = Search.find(params[:id])
    trans_builder
    trans_detail
    gen_email
    InvoiceMailer.gen_payment(@search,@gg,@email,@subtotal_value,@discount_value,@total_value).deliver_now

    respond_to do |format|
      @search.update_attributes(paid: true)
      format.html { redirect_to @search, notice: 'send' }
    end
  end

end
