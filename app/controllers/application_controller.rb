class ApplicationController < ActionController::Base
  Rails.application.routes.default_url_options[:host] = 'evhive.info'
  before_filter :configure_permitted_parameters, if: :devise_controller?


  protect_from_forgery with: :exception
  layout :layout_by_resource

  def layout_by_resource
    if user_signed_in?
      'dashboard'
    else
      'application'
    end
  end

  protected

  def configure_permitted_parameters
    devise_parameter_sanitizer.permit(:sign_up, keys: [:department_id, :fullname, :center_id])
  end
end
