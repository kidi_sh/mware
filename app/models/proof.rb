class Proof < ActiveRecord::Base
  belongs_to :budget
  has_attached_file :image, styles: { medium: "300x300>", thumb: "100x100>" },    :path => ":rails_root/public/images/:class/:attachment/:id/:style/:filename",:url => "/images/:class/:attachment/:id/:style/:filename"
   validates_attachment_content_type :image,  content_type: ['image/jpeg', 'image/png', 'image/gif', 'application/pdf']

end
