class Suborder < ActiveRecord::Base
  belongs_to :search

  def discount_rate
    if discount.nil?
      0
    else
      discount
    end
  end


end
