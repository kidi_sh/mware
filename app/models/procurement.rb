class Procurement < ActiveRecord::Base
  belongs_to :user
  belongs_to :center
  has_many :product_lists, inverse_of: :procurement,  dependent: :destroy
  accepts_nested_attributes_for :product_lists, reject_if: :all_blank, allow_destroy: true

  def status_message
    if status == 1
      "request"
    elsif status == 2
      "Approve By Head"
    elsif status == 3
      "Approve By Procurement"
    elsif status == 4
      "Approve By finance"
    end

  end
end
