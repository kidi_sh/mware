module SubordersHelper
    def options_for_product
      if current_user.center.name == "The Maja"
        [
          ["Private Office","[The Maja] Private Office"],
          ["Other Product","[The Maja] Others"],
          ["Event Weekday","[The Maja] Event Space Weekday"],
          ["Event Weekend","[The Maja] Event Space Weekend"],
          ["Meeting Room","[The Maja] Meeting Room"],
          ["Virtual Office","[The Maja] Virtual Office"],
          ["Daily Paas","[The Maja] Daily Pass"],
          ["Flexi Desk","Flexi Desk"]
        ]
      elsif current_user.center.name == "The Breeze"
        [
          ["Private Office","[The Breeze] Private Office"],
          ["Other Product","[The Breeze] Others"],
          ["Event Weekday","[The Breeze] Event Space Weekday"],
          ["Event Weekend","[The Breeze] Event Space Weekend"],
          ["Meeting Room","[The Breeze] Meeting Room"],
          ["Virtual Office","[The Breeze] Virtual Office"],
          ["Daily Paas","[The Breeze] Daily Pass"],
          ["Shared Space","[The Breeze] Monthly Shared Space"],
          ["Flexi Desk","Flexi Desk"]
        ]
      elsif current_user.center.name == "D'lab"
        [
          ["Private Office","[D.Lab] Private Office"],
          ["Water Bill","[D.Lab] Water"],
          ["Phone Bill","[D.Lab] Phone"],
          ["Lease Charge","[D.Lab] Lease Charge"],
          ["Electricity Bill","[D.Lab] Electricity"],
          ["Service Charge","[D.Lab] Service Charge"],
          ["Event Space","[D.Lab] Event Space"],
          ["Event Weekend","[D.Lab] Event Space Overtime"],
          ["Meeting Space","[D.Lab] Meeting Space"],
          ["Others","[D.Lab] Others"],
          ["Flexi Desk","Flexi Desk"],
          ["Food and Beverages","[D.Lab] Food and beverages"]
        ]
      elsif current_user.center.name == "JSC Hive"
        [
          ["Private Office","[JSC] Private Office"],
          ["Event Space","[JSC] Event Space"],
          ["Event Weekend","[JSC] Event Space Overtime"],
          ["Meeting Room","[JSC] Meeting Room"],
          ["Others","[JSC] Others"],
          ["Flexi Desk","Flexi Desk"]
        ]
      end
  end
end
