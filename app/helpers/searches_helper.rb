module SearchesHelper
  def trans_builder
    @gg = @search.transaction_list
  end

  def trans_detail
    @subtotal_value = @search.transaction_subtotal
    @discount_value = @search.transaction_discount
    @total_value = @search.transaction_total
  end

  def gen_email
    if Company.where(name: @search.company).empty?
    @email = Suborder.where(company: @search.company).pluck(:email).compact.first
    else
    @email = Company.where(name: @search.company).pluck(:email).join(', ')
    end
  end

  def cc_email
    if Company.where(name: @search.company).empty?
    @email = Suborder.where(company: @search.company).pluck(:email).compact.first
    else
    @email = Company.where(name: @search.company).pluck(:cc).join(', ')
    end
  end
end
