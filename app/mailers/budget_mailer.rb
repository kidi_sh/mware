class BudgetMailer < ApplicationMailer
  if Rails.env.production?
  def notify_finance(budget)
    @budget = budget
    if @budget.user.department.users.where('head = 1').present?
      if @budget.status = 1
        mail(:to => @budget.user.department.users.where('head = 1').pluck(:email).join(',') , :subject => "Budget Request from #{@budget.user.email}") do |format|
          format.text { render text: "New budget request from #{@budget.user.fullname}\n\nThank You \nEV HIVE" }
        end
      elsif @budget.status = 6
        mail(:to => "finance@evhive.co", :subject => "Budget Request from #{@budget.user.email}") do |format|
          format.text { render text: "New budget request from #{@budget.user.fullname}\n\nThank You \nEV HIVE" }
        end
      elsif @budget.status = 3
        mail(:to => "jason@evhive.co", :subject => "Budget Request from #{@budget.user.email}") do |format|
          format.text { render text: "New budget request from #{@budget.user.fullname}\n\nThank You \nEV HIVE" }
        end
      end
    else
      mail(:to => "finance@evhive.co", :subject => "Budget Request from #{@budget.user.email}") do |format|
        format.text { render text: "New budget request from #{@budget.user.fullname}\n\nThank You \nEV HIVE" }
      end
    end
  end

  def new_ot(overtime)
    @overtime = overtime
    mail(:to => "finance@evhive.co", :subject => "Overtime Request from #{@overtime.user.email}") do |format|
      format.text { render text: "New Overtime request from #{@overtime.user.fullname}\n\nClick here http://evhive.info/overtimes/#{@overtime.id}/edit\n\nThank You \nEV HIVE" }
    end
  end

  def ot_approve(overtime)
    @overtime = overtime
    mail(:to => @overtime.user.email, :subject => "Your overtime has been approve by from finance department") do |format|
      format.text { render text: "Your overtime request has been approved #{@overtime.user.fullname}\n\nThank You \nEV HIVE" }
    end
  end

  def rt_approve(overtime)
    @overtime = overtime
    mail(:to => @overtime.user.email, :subject => "Your overtime has been reject by from finance department") do |format|
      format.text { render text: "Your overtime request has been reject #{@overtime.user.fullname}\n\nThank You \nEV HIVE" }
    end
  end

  def trf(budget)
    @budget = budget
    @budget.each do | b |
    mail(:to => b.user.email, :subject => "The budget has been transfered to your account: #{b.user.email}") do |format|
      format.text { render text: "Your budget request has been transfered to your account:\n\n#{b.user.fullname}\n\nNotes\n\n#{b.note}\n\nFinance \nEV HIVE" }
    end
    end
  end

  def notify_user(budget)
    @budget = budget
    if @budget.status = 2
      mail(:to => @budget.user.email, :subject => "Budget Request revise: #{@budget.user.email}") do |format|
        format.text { render text: "Your budget request has been revise\n\n#{@budget.user.fullname}\n\nNotes\n\n#{@budget.note}\n\nFinance \nEV HIVE" }
      end
    elsif @budget.status = 3
      mail(:to => @budget.user.email, :subject => "Budget Request approved by finance: #{@budget.user.email}") do |format|
        format.text { render text: "Your budget request has been approve by finance\n\n#{@budget.user.fullname}\n\nNotes\n\n#{@budget.note}\n\nFinance \nEV HIVE" }
      end
    elsif @budget.status = 4
      mail(:to => @budget.user.email, :subject => "Budget Request spend: #{@budget.user.email}") do |format|
        format.text { render text: "Your budget request has been spend\n\n#{@budget.user.fullname}\n\nNotes\n\n#{@budget.note}\n\nFinance \nEV HIVE" }
      end
    elsif @budget.status = 5
      mail(:to => @budget.user.email, :subject => "Budget Request reject: #{@budget.user.email}") do |format|
        format.text { render text: "Your budget request has been reject\n\n#{@budget.user.fullname}\n\nNotes\n\n#{@budget.note}\n\nFinance \nEV HIVE" }
      end
    elsif @budget.status = 6
      mail(:to => @budget.user.email, :subject => "Budget Request approve by head: #{@budget.user.email}") do |format|
        format.text { render text: "Your budget request has been approve by head\n\n#{@budget.user.fullname}\n\nNotes\n\n#{@budget.note}\n\nFinance \nEV HIVE" }
      end
    elsif @budget.status = 7
      mail(:to => @budget.user.email, :subject => "Budget Request approve by CFO: #{@budget.user.email}") do |format|
        format.text { render text: "Your budget request has been approve by CFO\n\n#{@budget.user.fullname}\n\nNotes\n\n#{@budget.note}\n\nFinance \nEV HIVE" }
      end
    end
  end
else

  def trf(budget)
    @budget = budget
    @budget.each do | b |
    mail(:to => b.user.email, :subject => "The budget has been transfered to your account: #{b.user.email}") do |format|
      format.text { render text: "Your budget request has been transfered to your account:\n\n#{b.user.fullname}\n\nNotes\n\n#{b.note}\n\nFinance \nEV HIVE" }
    end
    end
  end

  def notify_finance(budget)
    @budget = budget
    mail(:to => "dunods@gmail.com", :subject => "Budget Request from #{@budget.user.email}") do |format|
      format.text { render text: "New budget request from #{@budget.user.fullname}\n\nThank You \nEV HIVE" }
    end
  end

  def notify_user(budget)
    @budget = budget
    if @budget.status = 2
      mail(:to => @budget.user.email, :subject => "Budget Request revise: #{@budget.user.email}") do |format|
        format.text { render text: "Your budget request has been revise\n\n#{@budget.user.fullname}\n\nNotes\n\n#{@budget.note}\n\nFinance \nEV HIVE" }
      end
    elsif @budget.status = 3
      mail(:to => @budget.user.email, :subject => "Budget Request approved: #{@budget.user.email}") do |format|
        format.text { render text: "Your budget request has been approve\n\n#{@budget.user.fullname}\n\nNotes\n\n#{@budget.note}\n\nFinance \nEV HIVE" }
      end
    elsif @budget.status = 4
      mail(:to => @budget.user.email, :subject => "Budget Request spend: #{@budget.user.email}") do |format|
        format.text { render text: "Your budget request has been spend\n\n#{@budget.user.fullname}\n\nNotes\n\n#{@budget.note}\n\nFinance \nEV HIVE" }
      end
    elsif @budget.status = 5
      mail(:to => @budget.user.email, :subject => "Budget Request reject: #{@budget.user.email}") do |format|
        format.text { render text: "Your budget request has been reject\n\n#{@budget.user.fullname}\n\nNotes\n\n#{@budget.note}\n\nFinance \nEV HIVE" }
      end
    end
  end
end
end
