class PerformanceSerializer < ActiveModel::Serializer
  attributes :id, :type, :description, :eta, :start, :end
end
