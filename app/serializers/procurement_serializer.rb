class ProcurementSerializer < ActiveModel::Serializer
  attributes :id, :purpose, :status
  has_one :user
  has_one :center
end
