class ProductSerializer < ActiveModel::Serializer
  attributes :id, :name, :jurnal_name
  has_one :center
end
