class OvertimeSerializer < ActiveModel::Serializer
  attributes :id, :date, :user_id, :note, :status, :center_id
end
