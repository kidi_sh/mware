class SuborderSerializer < ActiveModel::Serializer
  attributes :id, :start_date, :end_date, :total, :day, :company, :product_name
end
