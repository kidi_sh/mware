class CompanySerializer < ActiveModel::Serializer
  attributes :id, :name, :email
end
