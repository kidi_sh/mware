json.extract! overtime, :id, :date, :user_id, :note, :status, :center_id, :created_at, :updated_at
json.url overtime_url(overtime, format: :json)