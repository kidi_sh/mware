json.extract! department_role, :id, :name, :department_id, :created_at, :updated_at
json.url department_role_url(department_role, format: :json)