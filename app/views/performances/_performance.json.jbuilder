json.extract! performance, :id, :type, :description, :eta, :start, :end, :created_at, :updated_at
json.url performance_url(performance, format: :json)