json.extract! procurement, :id, :user_id, :center_id, :purpose, :status, :created_at, :updated_at
json.url procurement_url(procurement, format: :json)