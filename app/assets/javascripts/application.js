// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require highcharts
//= require chartkick
//= require jquery
//= require jquery_ujs
//= require_tree .
//= require cocoon
//= require dropzone
//= require twitter/bootstrap
//= require dataTables/jquery.dataTables
//= require dataTables/bootstrap/2/jquery.dataTables.bootstrap
//= require select2-full
//= require dataTables/extras/dataTables.responsive
//= require moment
//= require bootstrap-datetimepicker
//= require dataTables/extras/dataTables.buttons
//= require fancybox
jQuery(function() {
  $(".fancybox").fancybox();
});
