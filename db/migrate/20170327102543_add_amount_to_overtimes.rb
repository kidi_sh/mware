class AddAmountToOvertimes < ActiveRecord::Migration
  def change
    add_column :overtimes, :amount, :decimal
  end
end
