class AddNoteToSearches < ActiveRecord::Migration
  def change
    add_column :searches, :note, :string
  end
end
