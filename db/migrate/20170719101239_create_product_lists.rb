class CreateProductLists < ActiveRecord::Migration
  def change
    create_table :product_lists do |t|
      t.string :name
      t.decimal :price
      t.string :link
      t.boolean :approve
      t.belongs_to :procurement, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
