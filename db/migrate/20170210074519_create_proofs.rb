class CreateProofs < ActiveRecord::Migration
  def change
    create_table :proofs do |t|
      t.integer :budget_id

      t.timestamps null: false
    end
  end
end
