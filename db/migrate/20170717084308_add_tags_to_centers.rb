class AddTagsToCenters < ActiveRecord::Migration
  def change
    add_column :centers, :tag, :string
  end
end
