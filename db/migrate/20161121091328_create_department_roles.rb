class CreateDepartmentRoles < ActiveRecord::Migration
  def change
    create_table :department_roles do |t|
      t.string :name
      t.integer :department_id

      t.timestamps null: false
    end
  end
end
