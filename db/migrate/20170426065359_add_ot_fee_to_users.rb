class AddOtFeeToUsers < ActiveRecord::Migration
  def change
    add_column :users, :ot_fee, :decimal
  end
end
