class AddTaxandPayableToCenters < ActiveRecord::Migration
  def change
    add_column :centers, :tax, :string
    add_column :centers, :receivable, :string
  end
end
