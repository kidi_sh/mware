class CreateBudgets < ActiveRecord::Migration
  def change
    create_table :budgets do |t|
      t.date :date
      t.string :id_request
      t.integer :user_id
      t.integer :department_role_id
      t.text :details
      t.integer :quantity
      t.string :vendor
      t.decimal :amount
      t.decimal :cash_in
      t.decimal :cash_out
      t.decimal :balance

      t.timestamps null: false
    end
  end
end
