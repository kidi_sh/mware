class AddEndToOvertimes < ActiveRecord::Migration
  def change
    add_column :overtimes, :end, :datetime
  end
end
