class AddTypeToItems < ActiveRecord::Migration
  def change
    add_column :items, :type_product, :string
    add_column :items, :status, :integer
  end
end
