class AddVendorToItems < ActiveRecord::Migration
  def change
    add_column :items, :vendor, :string
  end
end
