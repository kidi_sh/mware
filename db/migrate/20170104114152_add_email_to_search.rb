class AddEmailToSearch < ActiveRecord::Migration
  def change
    add_column :searches, :email, :string
  end
end
