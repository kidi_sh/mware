class CreateItems < ActiveRecord::Migration
  def change
    create_table :items do |t|
      t.string :product
      t.integer :quantity
      t.decimal :price
      t.decimal :total
      t.integer :budget_id

      t.timestamps null: false
    end
  end
end
