class AddCenterToCompanies < ActiveRecord::Migration
  def change
    add_column :companies, :center, :string
  end
end
