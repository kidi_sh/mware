class AddCenterToSuborder < ActiveRecord::Migration
  def change
    add_column :suborders, :center, :string
  end
end
