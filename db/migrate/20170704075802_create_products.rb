class CreateProducts < ActiveRecord::Migration
  def change
    create_table :products do |t|
      t.string :name
      t.string :jurnal_name
      t.belongs_to :center, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
