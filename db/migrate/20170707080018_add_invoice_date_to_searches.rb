class AddInvoiceDateToSearches < ActiveRecord::Migration
  def change
    add_column :searches, :invoice_date, :datetime
  end
end
