class AddPaidToSearches < ActiveRecord::Migration
  def change
    add_column :searches, :paid, :boolean
  end
end
