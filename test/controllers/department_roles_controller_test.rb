require 'test_helper'

class DepartmentRolesControllerTest < ActionController::TestCase
  setup do
    @department_role = department_roles(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:department_roles)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create department_role" do
    assert_difference('DepartmentRole.count') do
      post :create, department_role: { department_id: @department_role.department_id, name: @department_role.name }
    end

    assert_redirected_to department_role_path(assigns(:department_role))
  end

  test "should show department_role" do
    get :show, id: @department_role
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @department_role
    assert_response :success
  end

  test "should update department_role" do
    patch :update, id: @department_role, department_role: { department_id: @department_role.department_id, name: @department_role.name }
    assert_redirected_to department_role_path(assigns(:department_role))
  end

  test "should destroy department_role" do
    assert_difference('DepartmentRole.count', -1) do
      delete :destroy, id: @department_role
    end

    assert_redirected_to department_roles_path
  end
end
